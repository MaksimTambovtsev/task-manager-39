package ru.tsc.tambovtsev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectSizeResponse extends AbstractResponse {

    private int size;

}
