package ru.tsc.tambovtsev.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sortType;

    public TaskListRequest(@Nullable String token, @Nullable Sort sortType) {
        super(token);
        this.sortType = sortType;
    }

}
