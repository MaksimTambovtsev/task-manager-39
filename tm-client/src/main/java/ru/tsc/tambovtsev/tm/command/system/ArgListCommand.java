package ru.tsc.tambovtsev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.model.ICommand;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String DESCRIPTION = "Show arguments list.";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand command : commands) {
            String argument = command.getArgument();
            if (argument != null && !argument.isEmpty())
                System.out.println(command.getArgument());
        }
    }

}
