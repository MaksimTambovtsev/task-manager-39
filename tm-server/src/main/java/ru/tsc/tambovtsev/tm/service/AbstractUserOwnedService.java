package ru.tsc.tambovtsev.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IOwnerRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IUserOwnedService;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.UserIdEmptyException;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

import java.util.Optional;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@Nullable final R repository, @NotNull IConnectionService connection) {
        super(repository, connection);
    }

    @NotNull
    public abstract IOwnerRepository<M> getRepository(@NotNull SqlSession connection);

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IOwnerRepository<M> repository = getRepository(sqlSession);
        try {
            repository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IOwnerRepository<M> repository = getRepository(sqlSession);
        try {
            @Nullable final M result = repository.findById(userId, id);
            return result;
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IOwnerRepository<M> repository = getRepository(sqlSession);
        try {
            @Nullable final int result = repository.getSize(userId);
            return result;
        }
        finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IOwnerRepository<M> repository = getRepository(sqlSession);
        try {
            @Nullable final M result = repository.findById(userId, id);
            if (result == null) return;
            repository.removeById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

}
