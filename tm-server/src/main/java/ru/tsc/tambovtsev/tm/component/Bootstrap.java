package ru.tsc.tambovtsev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.endpoint.*;
import ru.tsc.tambovtsev.tm.api.repository.IDomainRepository;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.*;
import ru.tsc.tambovtsev.tm.endpoint.*;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.repository.DomainRepository;
import ru.tsc.tambovtsev.tm.service.*;
import ru.tsc.tambovtsev.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IDatabaseProperty databaseProperties = new IDatabaseProperty() {
        @Override
        public String getDatabaseUsername() {
            return IDatabaseProperty.super.getDatabaseUsername();
        }

        @Override
        public String getDatabasePassword() {
            return IDatabaseProperty.super.getDatabasePassword();
        }

        @Override
        public String getDatabaseUrl() {
            return IDatabaseProperty.super.getDatabaseUrl();
        }
    };

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(databaseProperties);

    @NotNull
    private final IDomainRepository domainRepository = new DomainRepository(this);

    @Getter
    @NotNull
    private final SqlSession connection = connectionService.getSqlSession();

    @Getter
    @NotNull
    private final IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);

    @Getter
    @NotNull
    private final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);

    @Getter
    @NotNull
    private final IUserRepository userRepository = connection.getMapper(IUserRepository.class);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, connectionService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(
            projectRepository, taskRepository, connectionService
    );

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(
            connectionService, userRepository,  propertyService, projectRepository, taskRepository
    );

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(domainRepository);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final CalculatorEndpoint calculatorEndpoint = new CalculatorEndpoint();

    {
        registry(calculatorEndpoint);
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(domainEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    public void run() {
        createTable();
        initPID();
        initUsers();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
        backup.stop();
    }

    @SneakyThrows
    private void initUsers() {
        @Nullable User user = userService.findByLogin("test");
        if (user == null) userService.create("test", "test", "test@test.ru");
        user = userService.findByLogin("admin");
        if (user == null) userService.create("admin", "admin", Role.ADMIN);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "localhost";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?WSDL";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void createTable() {
        /*@NotNull final IConnectionService connectionService = new ConnectionService(databaseProperties);
        @NotNull final Connection connectionDB = connectionService.getConnection();
        @NotNull Statement statement = connectionDB.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS TM_PROJECT (" +
                " ID varchar(50) NOT NULL," +
                " NAME varchar(50) NOT NULL," +
                " DESCRIPTION varchar(50)," +
                " STATUS varchar(50) NOT NULL," +
                " USER_ID varchar(50) NOT NULL," +
                "  PRIMARY KEY (ID)" +
                ")";
        statement.executeUpdate(sql);
        connectionDB.commit();
        sql = "CREATE TABLE IF NOT EXISTS TM_TASK (" +
                " ID varchar(50) NOT NULL," +
                " NAME varchar(50) NOT NULL," +
                " DESCRIPTION varchar(50)," +
                " STATUS varchar(50) NOT NULL," +
                " USER_ID varchar(50) NOT NULL," +
                " PROJECT_ID varchar(50)," +
                "  PRIMARY KEY (ID)" +
                ")";
        statement.executeUpdate(sql);
        connectionDB.commit();
        sql = "CREATE TABLE IF NOT EXISTS TM_USER (" +
                " ID varchar(50) NOT NULL," +
                " LOGIN varchar(50) NOT NULL," +
                " EMAIL varchar(50)," +
                " PASSWORD_HASH varchar(50) NOT NULL," +
                " FIRSTNAME varchar(50)," +
                " LASTNAME varchar(50)," +
                " MIDDLENAME varchar(50)," +
                " ROLE varchar(50) NOT NULL," +
                " LOCKED varchar(50)," +
                "  PRIMARY KEY (ID)" +
                ")";
        statement.executeUpdate(sql);
        connectionDB.commit();
        statement.close();
        connectionDB.close();*/
    }

}