package ru.tsc.tambovtsev.tm.service;


import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.Project;

import static org.mybatis.dynamic.sql.SqlBuilder.*;
import static ru.tsc.tambovtsev.tm.dynamicsql.provider.ProjectProvider.*;

import java.util.*;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(
            @NotNull final IProjectRepository repository,
            @NotNull IConnectionService connection
    ) {
        super(repository, connection);
    }

    @NotNull
    @Override
    public IProjectRepository getRepository(@NotNull SqlSession connection) {
        return connection.getMapper(IProjectRepository.class);
    }

    @Override
    @SneakyThrows
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @NotNull final Project project = new Project();
            project.setUserId(userId);
            project.setName(name);
            project.setDescription(description);
            repository.create(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            final Project project = repository.findById(userId, id);
            if (project == null) return null;
            project.setName(name);
            project.setDescription(description);
            project.setUserId(userId);
            repository.updateById(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            final Project project = repository.findById(userId, id);
            if (project == null) return null;
            project.setStatus(status);
            project.setUserId(userId);
            repository.updateById(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return null;
    }

    @Override
    @SneakyThrows
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable final Project result = repository.findById(userId, id);
            if (result == null) return;
            repository.removeByIdProject(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(
            @Nullable final String userIdPr,
            @Nullable final Sort sort
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(sort).orElseThrow(NameEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            SqlColumn<?> sortColumn;
            if (sort.name().equals("NAME")){
                sortColumn = name;
            } else sortColumn = status;
            final SelectStatementProvider selectStatement =
                    select(id, userId, name, description, status).
                            from(project).
                            where(userId, isEqualTo(userIdPr)).
                            orderBy(sortColumn).
                            build().
                            render(RenderingStrategies.MYBATIS3);
            @Nullable final List<Project> result = repository.findAll(selectStatement);
            return result;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable final List<Project> result = repository.findAllProject();
            return result;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void addAll(@NotNull final Collection<Project> models) {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clearProject() {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clearProject();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Collection<Project> set(@NotNull final Collection<Project> models) {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clearProject();
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return models;
    }

}
