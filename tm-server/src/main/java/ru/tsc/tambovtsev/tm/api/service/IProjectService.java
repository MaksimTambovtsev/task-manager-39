package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    List<Project> findAll( @Nullable String userId, @Nullable Sort sort);

    void clearProject();

    @Nullable
    List<Project> findAll();

}
