package ru.tsc.tambovtsev.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IService;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.Optional;

public abstract class AbstractService<M extends AbstractEntity, R extends IRepository<M>> implements IService<M> {

    @Nullable
    protected final R repository;

    @NotNull
    protected final IConnectionService connection;

    @NotNull
    public abstract IRepository<M> getRepository(@NotNull SqlSession connection);

    @Override
    public abstract void addAll(@NotNull final Collection<M> models);

    public AbstractService(@Nullable R repository, @NotNull IConnectionService connection) {
        this.repository = repository;
        this.connection = connection;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IRepository<M> repository = getRepository(sqlSession);
        try {
            @Nullable final M result = repository.findById(id);
            return result;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final IRepository<M> repository = getRepository(sqlSession);
        try {
            @Nullable final M result = repository.findById(id);
            if (result == null) return;
            repository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
